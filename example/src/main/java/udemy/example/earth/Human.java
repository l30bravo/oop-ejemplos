package udemy.example.earth;

public class Human {
	
	String name;
	int age;
	int heigthInInches;
	String eyesColor;
	
	public Human() {
		
	}
	
	
	public Human(String name, int age, int heigthInInches, String eyesColor) {
		//this es una instancia del objeto
		this.name = name;
		this.age = age;
		this.heigthInInches = heigthInInches;
		this.eyesColor = eyesColor;
	}


	public void speak() {
		System.out.println("Hello, my name is "+name);
		System.out.println("I am "+heigthInInches+" inches tall");
		System.out.println("I am "+ age +" years old");
		System.out.println("My eyes color is "+eyesColor);
	}

	
	public void eat() {
		System.out.println("eating...");
	}
	
	public void walk() {
		System.out.println("walking...");
	}
	
	public void work() {
		System.out.println("working...");
	}
	
}
