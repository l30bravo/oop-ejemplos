package udemy.example.earth;

public class Earth {
	
	//TEST 2
	/*
	 * - Creacion de objetos con un constructor
	 */
	
	public void populate_earth() {
		Human human1 = new Human("Tom", 25, 75, "Blue");
		Human human2 = new Human("Joe", 28, 68, "Green");
	
		human1.speak();
		human2.speak();
	}
	
	//TEST 1
	/*
	 * - Crear objetos
	 * - Setear variables
	 */
	/*
	public void populate_earth() {
		Human tom;
		
		tom = new Human();
		tom.age=5;
		tom.eyesColor = "brown";
		tom.heigthInInches = 72;
		tom.name = "Tom Zsabo";				
		tom.speak();
		
		Human joe = new Human();
		joe.age=36;
		joe.eyesColor="Green";
		joe.heigthInInches=68;
		joe.name="Joe FFFF";
		
		joe.speak();
	}*/
	
}
