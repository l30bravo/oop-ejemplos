package udemy.example;

import udemy.example.animals.Animal;
import udemy.example.animals.Chiken;
import udemy.example.animals.Sparrow;
import udemy.example.animals.Fish;

/*
 * INERITANCE
 */
//import udemy.example.inheritance.Animal;
//import udemy.example.inheritance.Bird;


/*
 * EARTH && ANIMAL
 */
//import udemy.example.animals.Animal;
//import udemy.example.animals.Bird;
//import udemy.example.earth.Earth;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args ){
        
    	
    	//Chiken chiken1 = new Chiken(1, "M", 7);
    	
    	//Sparrow sparrow1 = new Sparrow(1,"F", 7);
    	Animal sparrow1 = new Sparrow(1,"F", 7);
    	
    	//Fish fish1 = new Fish(1, "M", 2);
    	Animal fish1 = new Fish(1, "M", 2);
    	
    	//sparrow1.move();
    	//fish1.move(); 
    	
    	move_animals(sparrow1);
    	move_animals(fish1);
    	
    	//Inheritance
    	/*
    	Animal animal1 = new Animal(12, "M", 23);
    	animal1.eat();
    	animal1.sleep();
    	
    	Bird bird1 = new Bird(3, "F", 10);
    	bird1.eat();
    	bird1.sleep();
    	bird1.fly();
    	*/
    	
    	//Zoo
    	/*
    	Animal animal1 = new Animal(12, "M", 23);
    	animal1.eat();
    	
    	Bird bird1 = new Bird();
    	bird1.fly();
    	*/
    	//Earth - Creacion de objetos
    	/*
    	Earth earth = new Earth();
    	earth.populate_earth();
    	*/
    }
    
    public static void move_animals(Animal animal) {
    	animal.move(); // para cualquier tipo de animal
    }
}
