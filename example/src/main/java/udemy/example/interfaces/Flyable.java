package udemy.example.interfaces;

/*
 * Todo lo que utilice esta interface esta obiligado a implementar los metodos.
 */
public interface Flyable {
	
	//abstract methods (abstraccion de la idea general)
	public void fly();

}
