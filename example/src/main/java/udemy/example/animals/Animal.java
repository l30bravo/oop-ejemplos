package udemy.example.animals;


/*
 * Una clase abstracta es creada para se implementada segun el caso
 */
public abstract class Animal {

	int age;
	String gender;
	int weightInLbs;
	
	public Animal(int age, String gender, int weightInLbs) {
		super();
		this.age = age;
		this.gender = gender;
		this.weightInLbs = weightInLbs;
	}
	
	public void eat() {
		System.out.println("eating ....");
	}
	
	public void sleep() {
		System.out.println("sleeping...");
	}
	
	public abstract void move();
}
